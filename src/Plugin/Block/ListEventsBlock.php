<?php

namespace Drupal\events_list\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\events_list\Controller\EventsController;

/**
* Provides a block with a simple text.
*
* @Block(
*   id = "list_event",
*   admin_label = @Translation("List Events Block"),
* )
*/
class ListEventsBlock extends BlockBase {

 /**
  * {@inheritdoc}
  */
  public function build() {
    $controller_variable = new EventsController;
    $rendering_in_block = $controller_variable->listAllEvents();
    return $rendering_in_block;
  }
}
